package set

import "reflect"

// Unique returns a set where every element only exists once
func (s Set) Unique() Set {
	u := Set{}
	for _, e := range s {
		if !IsIn(e, u) {
			u = append(u, e)
		}
	}
	return u
}

// Union returns a set that contains all things that are members of either A or B
func Union(a, b Set) Set {
	return append(a, b...).Unique()
}

// Intersection returns a set that contains all things that are members of both A and B
func Intersection(a, b Set) Set {
	intersection := Set{}
	for _, elementA := range a {
		for i, elementB := range b {
			if reflect.DeepEqual(elementA, elementB) {
				intersection = append(intersection, elementA)
				b = append(b[:i], b[i+1:]...)
			}
		}
	}
	return intersection
}

// Difference returns a set that contains all elements in A but not in B
func Difference(a, b Set) Set {
	c := Set{}
	for _, elementA := range a {
		for i, elementB := range b {
			if reflect.DeepEqual(elementA, elementB) {
				c = append(c, elementA)
				b = append(b[:i], b[i+1:]...)
			}
		}
	}
	return c
}
