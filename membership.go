package set

import "reflect"

// IsEqual tests wether A is equal to B
func IsEqual(a, b Set) bool {
	return reflect.DeepEqual(a, b)
}

// IsIn tests wether the element E is in the set A
func IsIn(e interface{}, a Set) bool {
	for _, elementA := range a {
		if reflect.DeepEqual(e, elementA) {
			return true
		}
	}
	return false
}

// IsSubset tests wether A is a subset of B
func IsSubset(a, b Set) bool {
	for _, elementA := range a {
		contains := false
		for _, elementB := range b {
			if elementA == elementB {
				contains = true
				break
			}
		}
		if !contains {
			return false
		}
	}
	return true
}

// Cardinality returns the number of unique members of the set
func (s Set) Cardinality() uint {
	return uint(len(s.Unique()))
}

// IsEmpty returns false when there's at least one element
func (s Set) IsEmpty() bool {
	if len(s) > 0 {
		return true
	}
	return false
}

// IsProperSubset tests wether A is a proper (or strict) subset of B
func IsProperSubset(a, b Set) bool {
	if IsEqual(a, b) && IsSubset(a, b) {
		return true
	}
	return false
}
