package set

import "reflect"

type Set []interface{}

// MakeSet returns the slice as set
func MakeSet(slice interface{}) Set {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("MakeSet() given a non-slice type")
	}

	ret := make(Set, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}
